import 'package:agrinvest/route_generator.dart';
import 'package:agrinvest/themes/theme.dart';
import 'package:agrinvest/widgets/custom_theme.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(
      CustomTheme(
        initialThemeKey: ThemeKeys.LIGHT,
        child: MyApp(),
      ),
  );
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'AgrInvest',
      initialRoute: HOME_ROUTE,
      onGenerateRoute: RouteGenerator.generateRoute,
      theme: CustomTheme.of(context),
      darkTheme: Themes.darkTheme,
    );
  }
}