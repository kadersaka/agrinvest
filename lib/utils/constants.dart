const PREFERENCE_SHOW_ONBOARD = "pref_show_onboard";
const PREFERENCE_DARK_THEME = "pref_dark_theme";
const PREFERENCE_PUSH_NOTIFICATIONS = "pref_push_notifications";
const PREFERENCE_LANGUAGE_INDEX = "pref_language_index";

const SYSTEM_LANGUAGE_PREFERENCE = -1;
