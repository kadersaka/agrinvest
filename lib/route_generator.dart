import 'package:agrinvest/screens/home_page.dart';
import 'package:flutter/material.dart';
const HOME_ROUTE = "/";


class RouteGenerator{
  static Route<dynamic> generateRoute(RouteSettings routeSettings){
    final args= routeSettings.arguments;
    switch(routeSettings.name){

      case HOME_ROUTE:
        return MaterialPageRoute(builder: (_) => Home());
      default:
        return MaterialPageRoute(
          builder: (_) => Scaffold(
            appBar: AppBar(
              title: Text('Error'),
            ),
            body: Center(
              child: Text('${routeSettings.name} not created yet'),
            ),
          ),
        );
    }
  }
}